var http = require('http');
var https = require('https');
var winston = require('winston');

winston.add(winston.transports.File, {
  filename: 'test.log'
});

var options = {
  method: 'HEAD',
  host: 'josephtate.com',
  port: 80,
  path: '/'
};

process.argv.forEach(function(val, index, array) {
  console.log(index + ': ' + val);
});

var counter = 1;

function loadTest () {
  if (counter < 5) {
    var req = http.request(options, function(res) {
      winston.info(res.statusCode);
    });
    req.end();
    counter++;
    setTimeout(loadTest, 10);
  }
};

loadTest();
